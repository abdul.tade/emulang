CXX := g++
CXXFLAGS := -O3 -Wall


.PHONY: all build run


all:
	make build
	make run


build: src/main.cpp build/ 
	$(CXX) $(CXXFLAGS) -g -o build/main src/main.cpp -fno-rtti -march=native -flto -fwhole-program -fprofile-generate -std=c++20


run: build/main
	./build/main


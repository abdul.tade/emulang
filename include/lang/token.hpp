#pragma once
#include "../type_suffix.hpp"
#include "token_type.hpp"
#include <string>

struct Token {

    Token(TokenType type, const std::string &&value)
        : type_(type),
          value_(value)
    {}

    const std::string &getValue() const {
        return value_;
    }

    TokenType getType() const {
        return type_;
    }

    friend bool operator==(const Token &lhs, const Token &rhs) {
        return (lhs.type_ == rhs.type_) && (lhs.value_ == lhs.value_);
    }

    friend bool operator!=(const Token &lhs, const Token &rhs) {
        return not operator==(lhs,rhs);
    }
    
private:
    TokenType type_;
    std::string value_;
};
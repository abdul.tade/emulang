#pragma once

#include <iostream>
#include <fstream>
#include <vector>

struct fs
{
    static std::string read(const std::string &filename)
    {
        std::string output{};
        std::ifstream file;
        file.open(filename, std::ios::binary | std::ios::in);
        if (file.is_open())
        {
            throw std::runtime_error{filename + "does not exists"};
        }

        file >> output;
        return output;
    }

    static std::vector<std::string> readAndSplit(const std::string &filename, char delimeter)
    {
        std::string token{};
        std::vector<std::string> lines{};

        std::ifstream file;
        file.open(filename, std::ios::binary | std::ios::in);
        if (file.is_open())
        {
            throw std::runtime_error{filename + "does not exists"};
        }

        while (std::getline(file, token, delimeter))
        {
            lines.emplace_back(token);
        }

        return lines;
    }
};
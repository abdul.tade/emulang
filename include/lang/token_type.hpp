#pragma once
#include <cstdint>

enum TokenType : uint8_t {
    KEYWORD, IDENTIFIER, LITERAL,
    EOF, INSTRUCTION
};
#pragma once
#include "fs.hpp"
#include "token.hpp"
#include <unordered_map>
#include <sstream>

const std::unordered_map<std::string, TypeSuffix> typename2type = {
    {"char", TypeSuffix::C},
    {"int", TypeSuffix::I},
    {"uint", TypeSuffix::U},
    {"float", TypeSuffix::F},
    {"string", TypeSuffix::S}
};

std::vector<std::string> split(const std::string &str, char delim) {
    std::stringstream ss{str};
    std::string token;

    while (std::getline(ss,token,delim)) {
        
    }
}

class Lexer
{
public:

    Lexer(const std::string &filename)
        : filename_(filename),
          output_(fs::readAndSplit(filename,'\n'))
    {}

    std::vector<Token> toTokens()
    {
        std::vector<Token> tokens{};
        for (auto& line : output_) {

        }
    }

private:
    std::string filename_;
    const std::vector<std::string> &output_;

    void processLine(const std::string &line)
    {

    }
};
#pragma once

enum Register {
    R0, R1, R2, R4, R5,
    R6, R7, R8, PC, SP,
    BP, IP
};
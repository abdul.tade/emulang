#pragma once
#include "type_suffix.hpp"
#include <string>
#include <cstring>
#include <unordered_map>

struct Symbol
{
    TypeSuffix type;
    void *value{nullptr};
};

using String = const std::string &;

class SymTab
{
public:
    SymTab() = default;

    SymTab(const SymTab &) = delete;
    SymTab(const SymTab &&) = delete;
    SymTab &operator=(const SymTab &) = delete;
    SymTab &operator=(const SymTab &&) = delete;

    ~SymTab()
    {
        for (auto [k, v] : table_)
        {
            if (v->type == TypeSuffix::S) {
                delete[] reinterpret_cast<char*>(v->value);
            }
            else {
                switch (v->type)
                {
                    case TypeSuffix::C:
                        delete reinterpret_cast<char*>(v->value);
                        break;
                    case TypeSuffix::U:
                        delete reinterpret_cast<unsigned int*>(v->value);
                        break;
                    case TypeSuffix::I:
                        delete reinterpret_cast<int*>(v->value);
                        break;
                    case TypeSuffix::F:
                        delete reinterpret_cast<float*>(v->value);
                        break;
                }
            }
            delete v;
        }
    }

    bool symbolExists(String name)
    {
        return table_.count(name) == 1;
    }

    void addSymbol(String str, TypeSuffix type)
    {
        if (symbolExists(str))
        {
            return;
        }

        table_[str] = new Symbol{type, nullptr};
    }

    template <typename T>
    int setValue(String name, T value)
    {
        if (not symbolExists(name))
        {
            return -1;
        }

        Symbol *symbol = table_[name];

        if (symbol->value != nullptr) {
            *reinterpret_cast<T*>(symbol->value) = value;
        }

        table_[name]->value = reinterpret_cast<void *>(new T{value});
        return 0;
    }

    int setValue(String name, const char *value)
    {
        if (not symbolExists(name))
        {
            return -1;
        }

        auto& symbol = table_[name];

        if (symbol->type != TypeSuffix::S)
        {
            return -2;
        }

        if (symbol->value != nullptr) {
            auto str = reinterpret_cast<char*>(symbol->value);
            auto len = strlen(str);
            if ((len == strlen(value)) && not std::strncmp(value, str, len)) {
                return 0;
            }
            else {
                delete[] str;
                auto len = strlen(value);
                char *newStr = new char[len + 1];
                std::memcpy(newStr, value, len);
                newStr[len] = '\0';
                table_[name]->value = reinterpret_cast<void *>(newStr);
            }
        }

        auto len = strlen(value);
        char *str = new char[len + 1];
        std::memcpy(str, value, len);
        str[len] = '\0';
        table_[name]->value = reinterpret_cast<void *>(str);

        return 0;
    }

    template <typename T>
    T *getSymbolValue(String name, TypeSuffix expectedType)
    {
        if (not symbolExists(name))
        {
            return nullptr;
        }

        if (expectedType != table_[name]->type) {
            return reinterpret_cast<T*>(-1);
        }
        return reinterpret_cast<T *>(table_[name]->value);
    }

    TypeSuffix getSymbolType(String name)
    {
        if (not symbolExists(name))
        {
            return TypeSuffix::E;
        }
        return table_[name]->type;
    }

private:
    std::unordered_map<std::string, Symbol *> table_;
};
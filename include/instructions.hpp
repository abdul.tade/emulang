#pragma once

#include <cstdint>

enum Instructions : uint8_t {
    ADD, SUB, MUL, DIV, FLAG,
    MOV, JMP, INC, DEC, SHL,
    SHR, XOR, AND, OR, DECL,
    DISP
};
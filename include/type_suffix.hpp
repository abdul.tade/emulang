#pragma once
#include <cstdint>

enum TypeSuffix : uint8_t {
    U, /*Unsigned int*/
    I, /*Int*/
    C, /*Char*/
    F, /*Float*/
    S, /*String*/
    E /*EOF*/
};
#include <iostream>
#include "../include/symbol_table.hpp"

int main() {
	SymTab symtab{};
	symtab.addSymbol("Boy",TypeSuffix::I);
	symtab.addSymbol("name", TypeSuffix::S);
	symtab.addSymbol("chr", TypeSuffix::C);
	
	symtab.setValue("chr",'$');
	symtab.setValue("Boy", 10);
	symtab.setValue("numfloat",10.0f);
	symtab.setValue("name","Abdul Hameed");

	auto value = symtab.getSymbolValue<int>("Boy", TypeSuffix::I);
	auto c = symtab.getSymbolValue<char>("Boy", TypeSuffix::C);
	
	symtab.setValue("name","Arkoh Sahene\n");
	std::cout << symtab.getSymbolValue<char>("name",TypeSuffix::S);
	std::cout << symtab.getSymbolValue<char>("chr",TypeSuffix::C) << '\n';
}